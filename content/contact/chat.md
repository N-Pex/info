---
id: 276
title: Live Chatrooms (IRC)
date: 2010-05-09T00:26:38-04:00
author: n8fr8
layout: page
guid: https://guardianproject.info/?page_id=276
menu:
  main:
    name: Chat on IRC
    parent: contact
---
Sign into the live chat room now by clicking on this link (it takes you to webchat.oftc.net): 

<a href="https://webchat.freenode.net/?randomnick=1&#038;channels=%23guardianproject&#038;uio=MT1mYWxzZSYyPXRydWUmND10cnVlJjk9dHJ1ZSYxMD10cnVlJjEyPXRydWU84" title="Freenode IRC Webchat" target="_blank"><img src="https://guardianproject.info/wp-content/uploads/2010/05/chat-150x150.jpg" alt="chat" width="150" height="150" class="size-thumbnail wp-image-12341" style="vertical-align:middle" srcset="https://guardianproject.info/wp-content/uploads/2010/05/chat-150x150.jpg 150w, https://guardianproject.info/wp-content/uploads/2010/05/chat-100x100.jpg 100w, https://guardianproject.info/wp-content/uploads/2010/05/chat-200x200.jpg 200w, https://guardianproject.info/wp-content/uploads/2010/05/chat.jpg 290w" sizes="(max-width: 150px) 100vw, 150px" /><strong style="font-size: 300%;">&nbsp;&nbsp;#guardianproject</strong></a>

If that one doesn&#8217;t work, try this one: <a href="https://webchat.oftc.net/?randomnick=1&#038;channels=guardianproject&#038;uio=MT1mYWxzZSYyPXRydWUmND10cnVlJjk9dHJ1ZSYxMD10cnVlJjExPTIwNSYxMj10cnVlb6#" title="OFTC IRC Webchat" target="_blank">#guardianproject</a>

<ul style="line-height:150%">
  <li>
    We welcome all questions, from the most beginner to advanced topics
  </li>
  <li>
    Just ask your question, you don&#8217;t need to ask permission first
  </li>
  <li>
    Be patient, it might take some time for someone to see and answer your question.
  </li>
  <li>
    We want your questions, and we try to answer as many as we can!
  </li>
</ul>

## Additional Options

Join us on IRC if you have a chat program installed:

<ul style="line-height:150%">
  <li>
    <a href="irc://irc.freenode.net/guardianproject" title="Guardian Project on IRC Freenode" target="_blank">#guardianproject on freenode</a> &#8211; the main chat room
  </li>
  <li>
    <a href="irc://irc.oftc.net/guardianproject" title="Guardian Project on IRC OFTC" target="_blank">#guardianproject on oftc</a> &#8211; alternate with easier Tor support
  </li>
</ul>

## Chat apps that use IRC

There are lots of apps that can be used with IRC chatrooms. Below are a couple recommendations for good, free chat apps for the major platforms.

  * **GNU/Linux** 
      * <a href="https://pidgin.im/" target="_blank">Pidgin</a>
      * <a href="https://hexchat.github.io/" target="_blank">HexChat</a>
  * **Apple OS X** 
      * <a href="http://colloquy.info/" target="_blank">Colloquy</a>
      * <a href="https://adium.im/" target="_blank">Adium</a>
  * **Windows** 
      * <a href="https://pidgin.im/" target="_blank">Pidgin</a>
      * <a href="https://hexchat.github.io/" target="_blank">HexChat</a>