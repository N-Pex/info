---
id: 248
title: Jobs and Internships
date: 2010-03-31T18:30:31-04:00
author: n8fr8
layout: page
guid: https://guardianproject.info/?page_id=248
menu:
  main:
    name: Work or Intern With Us
    parent: contact
---
[<img title="teamguardianhardatwork" src="https://guardianproject.info/wp-content/uploads/2010/03/teamguardianhardatwork.jpg" alt="" width="576" height="384" />  
](https://guardianproject.info/wp-content/uploads/2010/03/teamguardianhardatwork.jpg) _Don&#8217;t let the visual anonymity fool you &#8211; Team Guardian is hard at work and proud of it!_

### Jobs

**You** &#8211; We&#8217;re looking for developers of all levels to join us &#8211; primarily mobile application developers with skills ranging from user interface design down to the core app work at the network and data level. For experienced developers we expect you to have at least one app released in public and prefer that you have experience with open-source and community-led projects. Experience with security software and standards is desired, but not required. The ability to assist in the localization of software to different regions / languages is a plus.

**Responsibilities** &#8211; Dependent on seniority, but ideally you&#8217;ll be able to confidently take the reins of the overall project engineering processes &#8211; specifically software build and release, version control management and software testing. Act as the lead engineer and be responsible for pushing releases to app store(s), markets and other distribution points. Be the team&#8217;s primary internal contributor of original project code.

All work is contract-based and is not limited by geography.

If you are interested in the opportunities below, send a resume or link to relevant work [through one of our contact methods](https://guardianproject.info/contact/).

### Internships

We&#8217;re always on the look-out for energetic, enthusiastic, and capable interns.

**You** &#8211; While an aptitude for development is preferred, all that is required is a passion for mobile devices, an interest in security, the eagerness to learn and the ability to be self-directed and productive.

**The Opportunity** &#8211; You will be exposed to some of the most advanced work being done in the field of mobile privacy, while at the same time contributing to the general good of humanity and gaining real-world production development experience. If that isn&#8217;t enough, then you may also end up with a Guardian-enabled Android device of your own.

**Responsibilities** &#8211; Your work will include testing software, working with NGOs and activist groups to setup and use Guardian software, producing online tutorials and guides, and even social media promotion work.

Interested? [Learn how to contact us here](https://guardianproject.info/contact/) or email &#x6a;&#111;b&#x73;&#x40;gu&#x61;&#114;d&#x69;&#x61;np&#x72;&#111;j&#x65;&#x63;&#116;.&#x69;&#x6e;f&#x6f; with an introduction and resume.

### CURRENT OPEN POSITIONS

**December 2017: Citizen Media Outreach Coordinator**

We are looking for assistance in coordinating outreach and curriculum creation to promote the [Open Archive project](https://open-archive.net/), and similar apps we work on related to the secure publication and sharing of citizen generated content from mobile phones. Candidates should have excellent skills in writing and content creation, and experience with digital security training content and curriculum. Candidates should preferably have a background in journalism, library science or archiving.

**August 2017: Nearby Networks Developer (Hired, December!)**

We are looking to hire a mobile developer with significant experience in working with nearby radio communication technology like Bluetooth LE and Wifi P2P.

The task is to create an Open Source Android library that will make it easy to discover nearby devices (devices running the same “nearby” library) and share files through a simple user-friendly interface. This will be accomplished in the following ways:

  1. **Unified Library:** Create a single unified library to facilitate the discovery and sharing of files with nearby devices through Bluetooth classic, Bluetooth Low-Emission, WiFi LAN, WiFi Direct, and WiFi Infrastructure (“Aba”). The library will make it easy for developers to choose from a selection of peer-to-peer connection methods without having to worry about underlying implementation details.
  2. **Automation**: The library will be able to determine the most appropriate method of connecting to a peer(s) depending on the available hardware settings. Since this approach doesn’t specify the exact connection method it provides a more automated approach seamlessly switching to the various connection protocols as needed.

**User Interaction:** The library will allow users to call pre-made fragments that will provide an interface for users to discover and interact with nearby devices. The fragments can be styled & modified and dropped into a project for pre-made User-Interaction flows.

&nbsp;

**April 2017: Market Research and User Studies (CLOSED/HIRED)  
** 

Guardian Project is looking for someone who can help us with market and user research in Latin America on a contractual basis.

Job responsibilities include the following:

  * Assisting in survey distribution
  * Communication and coordination with Latin American users and organizations
  * Spanish Translation
  * Handling general tech support email help in Spanish
  * Weekly meetings
  * Interest in technology

**December 2016: Program / Engineering Manager with a dash of Dev Ops needed for open source mobile security project. (CLOSED/HIRED)  
** 

Guardian Project has a contract opening for someone who can improve our product build+test+release cycle for a mobile app at the cutting edge of usable security, combining privacy and security with usability and simplicity.

<div dir="ltr">
  <p class="defanged22-gmail-p1">
    <span class="defanged22-gmail-s1">You’ll provide coordination and encouragement to our far flung heroic team of developers, designers and testers. Experience with setting up automated mobile builds and testing through cloud device testing services is particularly helpful. </span>
  </p>
  
  <p class="defanged22-gmail-p1">
    <span class="defanged22-gmail-s1">If you’re super organized, detailed oriented, deadline driven, dependable, strive for the best and not satisfied with less, this contract is for you! </span>
  </p>
  
  <p class="defanged22-gmail-p1">
    <span class="defanged22-gmail-s1">Your focus will be on a rewarding sub-project of the Guardian Team, but participation in the larger mission and community is highly encouraged. We pay top of the line for an open source, grant funded project organization. The upside is you can literally live anywhere you wish [almost!], and other than a few scheduled scrums, set your own schedule.</span>
  </p>
  
  <p class="defanged22-gmail-p1">
    Underrepresented minorities in technology are especially encouraged to apply.
  </p>
  
  <hr />
</div>

&nbsp;