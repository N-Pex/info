---
id: 809
title: The Team
guid: https://guardianproject.info/?page_id=809
aliases:
  - /home/team/
menu:
  main:
    name: Our Team
    parent: home
---

Over the last four years, we&#8217;ve been slowly building up our team of core contributors to the project. Below you&#8217;ll find &#8220;privatized&#8221; photos of our core team, their IRC handles, and a bit about who they are and what they do for the project.

We&#8217;d also like to recognize the many contributions of patches, bug reports, feedback and ideas we have received from members of our larger community.

{{% team-list %}}
